import axios from "axios";


/* Creating an instance of axios with a baseURL of /api. */
const portfolioApi = axios.create({
    baseURL: `/api`,
})

export default portfolioApi;