import { IContact } from "../interfaces";
import { PortfolioApi } from "../api";



/**
 * It takes an object of type IContact, sends it to the server, and returns a boolean.
 * @param {IContact} info - IContact
 * @returns The data object is being returned.
 */

export const dataForm = async ( info: IContact ) => {

    const {data} =  await PortfolioApi.post('/contact', info);

    if(!data.ok){
        return false
    }

    return true;


}

