import type { NextApiRequest, NextApiResponse } from 'next'
import { db } from '../../db';
import { IContact } from '../../interfaces/contact';

import {ContactModel} from '../../models'

type Data = {
    ok: boolean
    msg: string
}


/**
 * It's a function that takes a request and a response, and returns a response.
 * 
 * The request is a NextApiRequest, and the response is a NextApiResponse.
 * 
 * The NextApiResponse is generic, and the generic type is Data.
 * 
 * The Data type is an interface that defines the shape of the data that will be returned in the
 * response.
 * 
 * The function is an async function, so it can use the await keyword.
 * 
 * The function uses a switch statement to determine which function to call based on the request
 * method.
 * 
 * The function returns a response.
 * 
 * The function is exported, so it can be imported by other files.
 * @param {NextApiRequest} req - NextApiRequest - The request object.
 * @param res - NextApiResponse<Data>
 * @returns the result of the switch statement.
 */
export default function handle (req: NextApiRequest, res: NextApiResponse<Data>) {

    switch (req.method) {
        case 'POST':
            return postContact( req, res );
       
        default:
            return res.status(400).json({
                ok: false,
                msg:`No Authorizado`
            })
    }

}

/**
 * It takes a request and a response, and then it does some stuff.
 * @param {NextApiRequest} req - NextApiRequest - The request object.
 * @param res - NextApiResponse<Data>
 */
const postContact = async (req: NextApiRequest, res: NextApiResponse<Data>) => {

    const data = req.body as IContact;

    try {

        db.connect();

        const add = new ContactModel(data);

        add.save();

        db.disconnect();

        res.status(201).json({
            ok: true,
            msg:`Add Entry`,
        })

        

    } catch (error) {
        
        res.status(500).json({
            ok: false,
            msg:`Fallo algo`
        })
    }


}
