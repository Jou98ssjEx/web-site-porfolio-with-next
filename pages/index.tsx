import type { NextPage } from 'next'

import { MainLayout, PorfolioContent } from '../components'

const Home: NextPage = () => {
  return (
    <>
      <MainLayout 
        title={'Portfolio App'} 
        section={''} 
        description={'HomePage'} 
      >
        <PorfolioContent />
      </MainLayout>
    </>
  )
}

export default Home
