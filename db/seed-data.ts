import { BorderColorOutlined, CodeOutlined, Facebook, GitHub, LinkedIn, StorageOutlined, ViewQuiltOutlined } from '@mui/icons-material';
import { IPortfolio } from '../interfaces/portfolio';

const URICloud =  `https://res.cloudinary.com/dgekgbspr/image/upload/v1634060588/certificates/`;

const URICloudP = `https://res.cloudinary.com/dgekgbspr/image/upload/v1653599752/projects/`


export const seedData: IPortfolio = {

    sectionHome: {
        info:{
            name: `Hi, I'am Joao`,
            profesion: `System Enginner`,
            description: `"Perhaps nothing in this world happens by accident. While everything happens for a reason, our destiny slowly takes shape."`,
            autor: `Silvers Rayleigh`,
            picture: `https://res.cloudinary.com/dgekgbspr/image/upload/v1634060588/Cv_dk0vo6.png`,
        },
        socialNetwork:[
            {
                name: 'LinkendIn',
                link: 'https://www.linkedin.com/in/joao-garc%C3%ADa-974725216/',
                icon: LinkedIn,
            },
            {
                name: 'Facebook',
                link: 'https://www.facebook.com/joaogabriel.garcia.96',
                icon: Facebook,
            },
            {
                name: 'GitLab',
                link: 'https://gitlab.com/Jou98ssjEx',
                icon: GitHub,
            },
        ],
    },
    sectionAbout:{
        info:{
            description:`I aspire to be a Full Stack developer with technologies like react and node, I have created small projects where I have applied my knowledge of self-learning on the Udemy platform, I find it exciting to create things with lines of code, I am currently a system engineer, applying more to web development, I am looking for challenges and new experiences.`,
            picture:`https://res.cloudinary.com/dgekgbspr/image/upload/v1653595313/resume/about_q9awgk.jpg`, 
        }
    },
    sectionSkills:[
        {
            icon: CodeOutlined,
            items:[
                {
                    skill: `JAVASCRIPT`,
                    porcent:`90`,
                },
                {
                    skill: `TYPESCRIPT`,
                    porcent:`75`,
                },
                {
                    skill: `REACT`,
                    porcent:`80`,
                },
                {
                    skill: `NEXTJS`,
                    porcent:`70`,
                },
            ],
            subTitle: ``,
            title:  `Frontend Developer`,
        },
        {
            title: `Backend Developer`,
            subTitle: ``,
            icon: StorageOutlined,
            items:[
                {
                    skill: `NodeJS`,
                    porcent:`85`,
                },
                {
                    skill: `MongoDb`,
                    porcent:`75`,
                },
                {
                    skill: `Firebase`,
                    porcent:`65`,
                },
               
            ]
        },
        {
            title: `Designer Web`,
            subTitle: ``,
            icon: ViewQuiltOutlined,
            items:[
                {
                    skill: `HTML`,
                    porcent:`90`,
                },
                {
                    skill: `CSS`,
                    porcent:`75`,
                },
                {
                    skill: `Bootstrap`,
                    porcent:`80`,
                },
                {
                    skill: `Material UI`,
                    porcent:`75`,
                },
            ]
        },
    ],
    sectionQuantificate:{
        education:[
            {
                title: `Basics`,
                subTitle: `School "Sorrowful Mother"`,
                time: `2004 - 2010`,
                link: ``
        
            },
            {
                title: `Sciences`,
                subTitle: `"San José" School`,
                time: `2010 - 2016`,
                link: `${URICloud}2016-02_Titulo_Bachiller_zqrysz.jpg`
        
            },
            {
                title: `Mobile app development`,
                subTitle: `Google Activate Platform`,
                time: `MAY - 2019`,
                link: `${URICloud}2019-05_Certificado_Desarrollo_de_app_tl55es.jpg`
        
            },
            {
                title: `Web design`,
                subTitle: `Udemy Platform`,
                time: `SEP - 2019`,
                link: `${URICloud}2019-09_Certificado_Diseño_web_imhmiy.jpg`
        
            },
            {
                title: `Digital competencies`,
                subTitle: `Google Activate Platform`,
                time: `DIC - 2020`,
                link: `${URICloud}2020-12_Certificado_Competemcias_Digitales_umlueo.jpg`
        
            },
            {
                title: `Digital marketing`,
                subTitle: `Google Activate Platform`,
                time: `DIC - 2020`,
                link: `${URICloud}2020-12_Certificado_Marketing_Digital_syk5xc.jpg`
        
            },
            {
                title: `Javascript Modern`,
                subTitle: `Udemy Platform`,
                time: `ABR - 2021`,
                link: `${URICloud}2021-04_Certificado_de_JavaScript_g28euw.jpg`
        
            },
            {
                title: `Node JS`,
                subTitle: `Udemy Platform`,
                time: `JUL - 2021`,
                link: `${URICloud}2021-07_Certificado_Node_js_blj3gw.jpg`
        
            },
            {
                title: `System Enginner`,
                subTitle: `University ULEAM`,
                time: `2016 - 2021`,
                link: `${URICloud}2021-07_Titulo_System_Enginner_García_Parrales_u6yzmz.jpg`
        
            },
            {
                title: `React JS`,
                subTitle: `Udemy Platform`,
                time: `MAY - 2022`,
                link: `${URICloud}2022-05_Certificado_React_h8ofka.jpg`
            },
            {
                title: `Next JS`,
                subTitle: `Udemy Platform`,
                time: `JUN - 2022`,
                link: `${URICloud}beyzjsdqffnrtavhbm15.jpg`
            },
        ],
        work:[
            {
                title: `IT Departament assistant`,
                subTitle: `Bilbosa Packer`,
                time: ` JUL - 2018`,
                link: `${URICloud}2018-09_Certificado_Prácticas_I_unxb0k.jpg`,
            },
            {
                title: `App Collaboration`,
                subTitle: `San Mateo Artisanal Fishing Port`,
                time: ` MAR - JUN 2020`,
                link: `${URICloud}2020-08_Certificado_Practicas_II_GARCIA_PARRALES_JOAO_fxqo55.jpg`,
            },
            {
                title: `Self-learning in Udemy virtual platform`,
                subTitle: `Udemy virtual platform`,
                time: ` 2019 - 2022`,
                link: ``,
            },
        ]
    },
    setionServices:[
        {
            icon: ViewQuiltOutlined,
            name: `Web`,
            lastName: `Designer`,
            items: [
                {
                    description: `Website layout`,
                },
                {
                    description: `Interface design`,
                },
                {
                    description: `Themes - fonts`,
                },
                {
                    description: `Responsive design`,
                },
            ]
        },
        {
            icon: CodeOutlined,
            name: `Frontend`,
            lastName: `Developer`,
            items: [
                {
                    description: `I developer the interface`,
                },
                {
                    description: `Consume API Rest`,
                },
                {
                    description: `Handle Hooks in React`,
                },
                // {
                //     description: `I position your company brand`,
                // },
            ]
        },
        {
            icon: BorderColorOutlined,
            name: `Backend`,
            lastName: `Developer`,
            items: [
                {
                    description: `I developer endpoint`,
                },
                {
                    description: `Handle Middlewares`,
                },
                {
                    description: `json Web Token - Auth`,
                },
                // {
                //     description: `I position your company brand`,
                // },
            ]
        },
    ],
    secionPortfolio:[
        {
            // img: `assets/img/portfolio1.jpg`,
            img: `${URICloudP}ihf9ajh28896crb3dysq.png`,
            name: `Online Store`,
            description: `Ecommerce - MongoDb - OAuth - Paypal - CRUD - middlewares`,
            link: `https://teslo-shop-ecomerce.herokuapp.com/`,
            time: `Jun 2022`,
    
        },
        {
            img: `${URICloudP}hackernew_nz1abr.png`,
            // img: `assets/img/portfolio3.jpg`,
            name: `Challenge hacker new`,
            description: `Challenge, Api Consumption, local storage of favorites, paging`,
            link: `https://coruscating-sorbet-d38fea.netlify.app/`,
            time: `May 2022`,
    
        },
        {
            // img: `assets/img/portfolio2.jpg`,
            img: `${URICloudP}openjira_t1juyo.png`,
            name: `Open Jira`,
            description: `Task App, CRUD, drag-and-drop functionality of the notes`,
            link: `https://open-jira-with-next-react-version-18.vercel.app/`,
            time: `Apr 2022`,
    
        },
       
        {
            // img: `assets/img/portfolio1.jpg`,
            img: `${URICloudP}polemon_vs4kne.png`,
            name: `Pokemon App`,
            description: `pokemon API consumption, local storages, show favorites, developed in Nextjs`,
            link: `https://pokemon-app-nextjs-xi.vercel.app/`,
            time: `Apr 2022`,
    
        },
        {
            // img: `assets/img/portfolio1.jpg`,
            img: `${URICloud}funplay_srxwbh.png`,
            name: `FunPlay App`,
            description: `Software for children of the elementary basic section, which allows the reinforcement of basic operations through interactive games.`,
            link: `https://funplayv2.herokuapp.com/`,
            time: `Jan 2021`,
    
        },
    ]
}