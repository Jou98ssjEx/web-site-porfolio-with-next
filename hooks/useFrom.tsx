import { ChangeEvent, useState } from 'react'



// export function useForm<T> (initialState: T) {


/**
 * It's a hook that takes an initial state and returns an object with the form state, a function to
 * handle changes, a function to reset the form, and a function to set the form
 * @param {T} initialState - The initial state of the form.
 */

export const useForm = <T extends Object > (initialState : T) => {


    const [form, setForm] = useState(initialState);

    const reset = () =>{
        setForm(initialState);
    }

    const handleChangeInput = ( { target } : ChangeEvent<HTMLInputElement |HTMLTextAreaElement>  ) => {

        setForm( {
            ...form,
            [target.name] : target.value
        })
    }

    return {form, handleChangeInput, reset, setForm}
}
