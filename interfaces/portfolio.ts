import { SvgIconTypeMap } from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";

export type IStringIcon = OverridableComponent<SvgIconTypeMap<{}, "svg">> & { muiName: string; };


export interface IPortfolio {

    sectionHome: ISectionHome;
    sectionAbout: ISectionAbout;
    sectionSkills: ISectionSkills[];
    sectionQuantificate: ISectionQuantificate;
    setionServices: ISectionServices[];
    secionPortfolio: ISectionPortfolio[];

}

export interface ISectionHome{
    info:{
        name: string;
        lastName?: string;
        profesion: string;
        description: string;
        autor: string;
        picture: string;
    },
    socialNetwork:
        {
            name: string;
            link: string;
            icon: IStringIcon,
        }[]
}

export interface ISectionAbout{
    info:{
        picture: string;
        description: string;
    }
}

export interface ISectionSkills{
    title: string;
    subTitle: string;
    icon: IStringIcon;
    items: {
        skill: string, // mayuscula
        porcent: string,
    }[];
}

export interface ISectionQuantificate{
    education:{
        title: string;
        subTitle: string;
        time: string;
        link: string;
    }[],
    work:{
        title: string;
        subTitle: string;
        time: string;
        link: string;
    }[],
}

export interface ISectionServices{
        icon: IStringIcon;
        name: string;
        lastName: string;
        items: {
            description: string;
        }[];
}

export interface ISectionPortfolio{
    img: string;
    name: string;
    description: string;
    link: string;
    time: string;
}