

/* Defining the interface for the contact form. */
export interface IContact {
    name     : string;
    email    : string;
    project  : string;
    message  : string;
}