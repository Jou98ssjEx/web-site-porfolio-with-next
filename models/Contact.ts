import mongoose, { Schema, model, Model } from 'mongoose'
import { IContact } from '../interfaces';

/* Creating a schema for the contact model. */
const contactSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true},
    project: { type: String, required: true },
    message: { type: String, required: true },
},
{
    timestamps: true, // create and update
});

/* Checking if the model exists, if it does, it will use it, if it doesn't, it will create it. */
const Contact: Model<IContact> = mongoose.models.Contact || model('Contact', contactSchema);

export default Contact;

