// import mongoose, { Schema, model, Model } from 'mongoose'
// import { IContact } from '../interfaces';

// /* Creating a schema for the contact model. */
// const homeSchema = new Schema({
//     name: { type: String, required: true },
//     email: { type: String, required: true},
//     project: { type: String, required: true },
//     message: { type: String, required: true },
// },
// {
//     timestamps: true, // create and update
// });

// /* Checking if the model exists, if it does, it will use it, if it doesn't, it will create it. */
// const Home: Model<IContact> = mongoose.models.Home || model('Home', homeSchema);

// export default Home;

import mongoose, { Schema, model, Model } from 'mongoose'
import { ISectionHome, IStringIcon } from '../interfaces';

/* Creating a schema for the contact model. */
const NameSchema = new Schema({
    // email: { type: String, required: true},
    // project: { type: String, required: true },
    // message: { type: String, required: true },
    info:{
        autor       : { type: String, required: true },
        description : { type: String, required: true },
        lastName    : { type: String, required: false },
        name        : { type: String, required: true },
        picture     : { type: String, required: true },
        profesion   : { type: String, required: true },

    },
    socialNetwork:[{
        name: { type: String, required: true},
        link: { type: String, required: true},
        icon: { type: String, required: true},
    }]
},
{
    timestamps: true, // create and update
});

/* Checking if the model exists, if it does, it will use it, if it doesn't, it will create it. */
const Home: Model<ISectionHome> = mongoose.models.Home || model('Home', NameSchema);

export default Home;
