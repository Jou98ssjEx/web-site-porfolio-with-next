import React, { ChangeEvent, FormEvent } from 'react'

import PhoneOutlinedIcon from '@mui/icons-material/PhoneOutlined';
import MailOutlineOutlinedIcon from '@mui/icons-material/MailOutlineOutlined';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import SendOutlinedIcon from '@mui/icons-material/SendOutlined';

import { useSnackbar } from 'notistack';
import { useForm } from '../../../hooks';
import { useState } from 'react';
import { validations } from '../../../helpers';
import { dataForm } from '../../../helpers/dataForm';
import { IContact } from '../../../interfaces';
import { db } from '../../../db';

const information = [
    {
        title: `Cal Me`,
        content: `+593 96 361 8740`,
        icon: <PhoneOutlinedIcon className='contact__icon' />,
    },
    {
        title: `Email`,
        content: `jass2980@outlook.es`,
        icon: <MailOutlineOutlinedIcon  className='contact__icon'/>,
    },
    {
        title: `Location`,
        content: `Online`,
        icon: <LocationOnOutlinedIcon className='contact__icon' />,
    },
]

type FromData = {
    name: string,
    email: string,
    project: string,
    msg: string,
}


export const ContactSection = () => {

    const inital: IContact = {
        name: '',
        email: '',
        project: '',
        message: '',
    }

    const {form, handleChangeInput, reset, setForm} = useForm<IContact>(inital);

    const { email, message, name ,project } = form;

    // const [error, setError] = useState('');

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    
    
    const handleSubmit = async ( e: FormEvent<HTMLFormElement>) => {
            e.preventDefault();
            if(isFormValid()){
                // console.log(form)
                  

                const resp = await dataForm(form);

                if(!resp){
                    enqueueSnackbar('Error Server', {
                        variant: 'error',
                        autoHideDuration: 2000,
                        anchorOrigin:{
                            vertical: 'top',
                            horizontal: 'right',
                        }
                      })
                }

                enqueueSnackbar('Send', {
                    variant: 'success',
                    autoHideDuration: 2000,
                    anchorOrigin:{
                        vertical: 'top',
                        horizontal: 'right',
                    }
                  })
            }

            // await db.connect();
    }

    // validacion del Form
    const isFormValid = () => {
        const msg = ` is required`;

        if (name.trim().length === 0 ){
            snackAlert(`Name ${msg}`);
            return false;
        } else if (email.trim().length === 0 ){
            snackAlert(`Email ${msg}`);
            return false;
        } else if(!validations.isValidEmail(email)){
            snackAlert(`Email no valid`);
            return false;
        }
        else if (project.trim().length === 0 ){
            snackAlert(`Project ${msg}`);
            return false;
        } else if ( message.trim().length === 0 ){
            snackAlert(`Message ${msg}`);
            return false;
        }

        // setError(``);
        reset();
        return true;

    }

    const snackAlert = ( messageError : string ) => {
        enqueueSnackbar(messageError, {
            variant: 'error',
            autoHideDuration: 2000,
            anchorOrigin:{
                vertical: 'top',
                horizontal: 'right',
            }
          });
    }


  
  


  return (
    <>
         {/* <!--==================== CONTACT ME ====================--> */}
         <section className="contact section" id="contact">
            <h2 className="section__title"> Contact Me </h2>
            <span className="section__subtitle"> Get in touch </span>

            <div className="contact__container container grid">

                <div className="">
                    {
                        information.map( d => (
                            <div
                                key={d.title}
                                className='contact__information'
                            >
                                {d.icon}
                                <div className="">
                                    <h3 className="contact__tittle"> {d.title} </h3>
                                    <span className="contact_subtitle"> {d.content} </span>
                                </div>

                            </div>
                        ))
                    }
                 
                </div>

             
                <form 
                    noValidate // desactiva valodate html
                    onSubmit={handleSubmit}
                    className="contact__form grid"
                >
                            {/* {
                                error && (
                                    <div className='error__msg'>
                                        <p> {error} </p>
                                    </div>
                                    )
                            } */}


                    <div className="contact__inputs grid">
                        <div className="contact__content">
                            <label className="contact__label">Name</label>
                            <input 
                                type="text" 
                                className="contact__input"
                                name={`name`}
                                value={name}
                                onChange={ handleChangeInput}
                                // autoFocus={true}
                               
                            />
                        </div>

                        <div className="contact__content">
                            <label className="contact__label">Email</label>
                            <input 
                                type="email" 
                                className="contact__input" 
                                name={`email`}
                                value={email}
                                onChange={ handleChangeInput}

                            />
                        </div>
                    </div>

                    <div className="contact__content">
                        <label className="contact__label">Project</label>
                        <input 
                            type="text" 
                            className="contact__input" 
                            name={`project`}
                            value={project}
                            onChange={ handleChangeInput}
                        />
                    </div>

                    <div className="contact__content">
                        <label className="contact__label">Message</label>
                        <textarea 
                            // name="" 
                            id="" 
                          // cols="0" 
                          // rows="7" 
                          className="contact__input"
                          name={`message`}
                          value={message}
                          onChange={ handleChangeInput}
                          
                        >
                        </textarea >
                    </div>

                    <div className="">
                        <button 
                            type='submit'
                            style={{
                                border: 'none',
                            }}
                            className="button button--flex"
                            // disabled={true}
                        >
                            Send
                            <SendOutlinedIcon className='button__icon'/>
                        </button>
                    </div>
                </form>
            </div>

        </section>
    </>
  )
}
