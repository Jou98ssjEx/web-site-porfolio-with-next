import Image from 'next/image'

import {
    FileDownloadOutlined,
} from '@mui/icons-material';


const data={
    picture: `https://res.cloudinary.com/dgekgbspr/image/upload/v1653595313/resume/about_q9awgk.jpg`,
    // picture: `https://res.cloudinary.com/dgekgbspr/image/upload/v1653595821/resume/about2_abz7kc.jpg`,
    // picture: `assets/img/about.jpg`,
    // description: `You have to accept the fact that you are not always the best, however having the will to fight for what you want, makes you better every day, that's why to achieve what I propose to myself in any cause or calamity, my challenge is not to give up, only then I can be better than yesterday.`
    description: `I aspire to be a Full Stack developer with technologies like react and node, I have created small projects where I have applied my knowledge of self-learning on the Udemy platform, I find it exciting to create things with lines of code, I am currently a system engineer, applying more to web development, I am looking for challenges and new experiences.`
    // description: `To give the best of myself, to learn more about my profession every day, being respectful, responsible and acting with prudence. To achieve every goal and challenge I set for myself.`
    // description: `Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolor quod commodi molestias vero amet, perspiciatis eum cumque, unde blanditiis distinctio repellat optio magni adipisci fuga nostrum voluptatem quisquam. Eum, illo!`
}

const info = [
    {
        // lastTime: <EmojiObjectsOutlined />,
        lastTime: <Image height={30} width={30}  className='icon__aboutI' src="/assets/img/react-js.svg" alt="react" />,
        text: `Reactjs`,
        status: ` `,
    },
    {
        // lastTime: <AutoStoriesOutlined /> ,
        lastTime: <Image height={30} width={30}  className='icon__aboutI' src="/assets/img/node-js.svg" alt="node" />,
        text: `Nodejs`,
        status: ``,
    },
    {
        // lastTime: <ReportProblemOutlined />,
        lastTime: <> <Image height={30} width={30}  className='icon__aboutI' src="/assets/img/js.svg" alt="js" /> <Image height={30} width={30}  className='icon__aboutI' src="/assets/img/type.svg" alt="ts" /> </>,
        text: `Languages`,
        status: ``,
    },
]

export const AboutSection = () => {

  return (
      <>
         {/* <!--==================== ABOUT ====================--> */}
         <section className="about section" id="about">
            <h2 className="section__title">
                About Me
            </h2>
            <span className="section__subtitle">
                My Introduccion
            </span>

            <div className="about__container container grid">
                <img src={data.picture} alt="img" className="about__img"/>

                <div className="about__data">
                    <p className="about__description">
                        {data.description}
                    </p>

                    <div className="about__info">
                        {
                            info.map( data => (
                                <div
                                    key={data.text}
                                >
                                    <span className="about__info-title">
                                        {data.lastTime}
                                        
                                        {/* <UsbRounded /> */}
                                    </span>
                                    <span className="about__info-name">
                                        {data.text} <br/> {data.status}
                                    </span>
                                </div>
                            ))
                        }
                    </div>

                    <div className="about__buttons">
                        <a 
                            // download="/assets/pdf/myResume.pdf" 
                            target={'_blank'}
                            rel='noreferrer'
                            href="assets/pdf/myResume.pdf" 
                            className="button button--flex"
                        >
                           Download CV 
                            <FileDownloadOutlined className='button__icon'/>
                        </a>
                    </div>
                </div>
            </div>
         </section>
      </>
  )
}
