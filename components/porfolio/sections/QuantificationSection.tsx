import React, { useState } from 'react'

import SchoolIcon from '@mui/icons-material/School';
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';

import CalendarMonthOutlinedIcon from '@mui/icons-material/CalendarMonthOutlined';

const URICloud =  `https://res.cloudinary.com/dgekgbspr/image/upload/v1634060588/certificates/`;

const education = [
    {
        title: `Basics`,
        subTitle: `School "Sorrowful Mother"`,
        time: `2004 - 2010`,
        link: ``

    },
    {
        title: `Sciences`,
        subTitle: `"San José" School`,
        time: `2010 - 2016`,
        link: `${URICloud}2016-02_Titulo_Bachiller_zqrysz.jpg`

    },
    {
        title: `Mobile app development`,
        subTitle: `Google Activate Platform`,
        time: `MAY - 2019`,
        link: `${URICloud}2019-05_Certificado_Desarrollo_de_app_tl55es.jpg`

    },
    {
        title: `Web design`,
        subTitle: `Udemy Platform`,
        time: `SEP - 2019`,
        link: `${URICloud}2019-09_Certificado_Diseño_web_imhmiy.jpg`

    },
    {
        title: `Digital competencies`,
        subTitle: `Google Activate Platform`,
        time: `DIC - 2020`,
        link: `${URICloud}2020-12_Certificado_Competemcias_Digitales_umlueo.jpg`

    },
    {
        title: `Digital marketing`,
        subTitle: `Google Activate Platform`,
        time: `DIC - 2020`,
        link: `${URICloud}2020-12_Certificado_Marketing_Digital_syk5xc.jpg`

    },
    {
        title: `Javascript Modern`,
        subTitle: `Udemy Platform`,
        time: `ABR - 2021`,
        link: `${URICloud}2021-04_Certificado_de_JavaScript_g28euw.jpg`

    },
    {
        title: `Node JS`,
        subTitle: `Udemy Platform`,
        time: `JUL - 2021`,
        link: `${URICloud}2021-07_Certificado_Node_js_blj3gw.jpg`

    },
    {
        title: `System Enginner`,
        subTitle: `University ULEAM`,
        time: `2016 - 2021`,
        link: `${URICloud}2021-07_Titulo_System_Enginner_García_Parrales_u6yzmz.jpg`

    },
    {
        title: `React JS`,
        subTitle: `Udemy Platform`,
        time: `MAY - 2022`,
        link: `${URICloud}2022-05_Certificado_React_h8ofka.jpg`
    },
    {
        title: `Next JS`,
        subTitle: `Udemy Platform`,
        time: `JUN - 2022`,
        link: `${URICloud}beyzjsdqffnrtavhbm15.jpg`
    },
]


const work = [
    {
        title: `IT Departament assistant`,
        subTitle: `Bilbosa Packer`,
        time: ` JUL - 2018`,
        link: `${URICloud}2018-09_Certificado_Prácticas_I_unxb0k.jpg`,
    },
    {
        title: `App Collaboration`,
        subTitle: `San Mateo Artisanal Fishing Port`,
        time: ` MAR - JUN 2020`,
        link: `${URICloud}2020-08_Certificado_Practicas_II_GARCIA_PARRALES_JOAO_fxqo55.jpg`,
    },
    {
        title: `Self-learning in Udemy virtual platform`,
        subTitle: `Udemy virtual platform`,
        time: ` 2019 - 2022`,
        link: ``,
    },
]



export const QuantificationSection = () => {

    const [view, setView] = useState(true);

    const handleChange = () => {
        setView(!view)
    }

  return (
    <>
        {/* <!--==================== QUALIFICATION ====================--> */}
        <section className="qualification section" id='qualification'>
            <h2 className="section__title"> Qualification </h2>
            <span className="section__subtitle"> My personal journey </span>

            <div className="qualification__container container">
                <div className="qualification__tabs">
                    <div 
                        // className="qualification__button button--flex qualification__active" 
                        className={ view ? `qualification__button button--flex qualification__active`: `qualification__button button--flex`} 
                        data-target='#education'
                        onClick={handleChange}
                    >
                        <SchoolIcon className='qualification__icon' />
                        Education
                    </div>

                    <div 
                        // className="qualification__button button--flex" 
                        className={ !view ? `qualification__button button--flex qualification__active`: `qualification__button button--flex`} 
                        data-target='#work'
                        onClick={handleChange}
                    >
                        <BusinessCenterIcon className='qualification__icon'/>
                        Work
                    </div>
                </div>

                <div className="qualification__sections">

                    {
                        view ? (

                            education.map( (d, idx) => 

                                (
                                    <div 
                                        className={view ? `qualification__active` :`qualification__content `} 
                                        data-content id="education"
                                        key={d.title}
                                    >
                                        <div className="qualification__data">
                                        {
                                               ((idx+1)%2===0) &&
                                                (
                                                    <>
                                                        <div className=""></div>
                                                        <div className="">
                                                            <span className="qualification__rounder"/>
                                                            <span 
                                                                className="qualification__line"
                                                                style={{
                                                                    display: education.length === (idx+1) ? 'none' :'flex',
                                                                }}
                                                            />
                                                        </div>
                                                    </>
                                                )
                                        }
                                            <a 
                                                className=""
                                                target={`_blank`}
                                                rel=''
                                                href={d.link ? d.link: ''}
                                                style={{
                                                    color: 'black'
                                                    // poner variable global
                                                }}
                                            >
                                                <h3 className="qualification__title">
                                                    {d.title}
                                                </h3>
                                                <span className="qualification__subtitle">
                                                    {d.subTitle}
                                                </span>
                                                <div className="qualifiaction__calendar">
                                                    <CalendarMonthOutlinedIcon />
                                                    {d.time}
                                                </div>
                                            </a>

                                        {
                                            ((idx+1)%2!==0) &&
                                                (
                                                    <div className="">
                                                        <span className="qualification__rounder"/>
                                                        <span 
                                                            className="qualification__line"
                                                            style={{
                                                                display: education.length === (idx+1) ? 'none' :'flex',
                                                            }}
                                                        />
                                                    </div>
                                                )
                                        }
                                        </div>
                                    </div>
                                )


                            )
                               
                        ) : (

                            work.map( (d, idx) => (
                                <div 
                                    className={!view ? `qualification__active` :`qualification__content `} 
                                    data-content 
                                    id="work"
                                    key={d.title}
                                >
                                    <div className="qualification__data">
                                        {
                                            ((idx+1)%2===0) &&
                                                (
                                                    <>
                                                        <div className=""></div>
                                                        <div className="">
                                                            <span className="qualification__rounder"/>
                                                            <span 
                                                                className="qualification__line"
                                                                style={{
                                                                    display: work.length === (idx+1) ? 'none' :'flex',
                                                                }}
                                                            />
                                                        </div>
                                                    </>
                                                )
                                        }
                                         <a 
                                            className=""
                                            target={'_blank'}
                                            rel='noreferrer'
                                            style={{
                                                color: 'black'
                                            }}
                                            href={d.link ? d.link : '/'}
                                         >
                                            <h3 className="qualification__title">
                                                {d.title}
                                            </h3>
                                            <span className="qualification__subtitle">
                                                {d.subTitle}
                                            </span>
                                            <div className="qualifiaction__calendar">
                                                <CalendarMonthOutlinedIcon />
                                                {d.time}
                                            </div>
                                        </a>

                                        {
                                            ((idx+1)%2!==0) &&
                                                (
                                                    <div className="">
                                                        <span className="qualification__rounder"/>
                                                        <span 
                                                            className="qualification__line"
                                                            style={{
                                                                display: work.length === (idx+1) ? 'none' :'flex',
                                                            }}
                                                        />
                                                    </div>
                                                )
                                        }
                                    </div>
                                </div>
                            ))
                            
                       )
                    }
                 

                </div>

            </div>

        </section>
    </>
  )
}
