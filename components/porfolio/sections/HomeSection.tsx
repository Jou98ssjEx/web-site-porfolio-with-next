import React from 'react'
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import MouseIcon from '@mui/icons-material/Mouse';
import SendIcon from '@mui/icons-material/Send';

import {Link} from 'react-scroll'
import { seedData } from '../../../db';

const { sectionHome } = seedData;
const { info, socialNetwork } = sectionHome;

// const data = {
//     name: `Hi, I'am Joao`,
//     profesion: `System Enginner`,
//     description: `"Perhaps nothing in this world happens by accident. While everything happens for a reason, our destiny slowly takes shape."`,
//     autor: `Silvers Rayleigh`,
//     picture: `https://res.cloudinary.com/dgekgbspr/image/upload/v1634060588/Cv_dk0vo6.png`,
// }

// const networkSocial =[
//     {
//         name: 'LinkendIn',
//         link: 'https://www.linkedin.com/in/joao-garc%C3%ADa-974725216/',
//         icon: <LinkedInIcon />,
//     },
//     {
//         name: 'Facebook',
//         link: 'https://www.facebook.com/joaogabriel.garcia.96',
//         icon: <FacebookIcon />,
//     },
//     {
//         name: 'GitLab',
//         link: 'https://gitlab.com/Jou98ssjEx',
//         icon: <GitHubIcon />,
//     },
// ]

export const HomeSection = () => {
  return (
    <>
         {/* <!--==================== HOME ====================--> */}
         <section className="home section" id="home">
            <div className="home__container container grid">
                <div className="home__content grid">
                    <div className="home__social">

                        {
                            socialNetwork.map( data => (
                                <a
                                    key={data.name}
                                    className={`home__social-icon`}
                                    target='_blank'
                                    rel='noreferrer'
                                    href={data.link}
                                 >
                                    {<data.icon/>} 
                                </a>
                            ))
                        }


                    </div>
                    <div className="home__img">
                        <svg className="home__blob" viewBox="0 0 200 187" xmlns="http://www.w3.org/2000/svg" 
                        // xmlns:xlink="http://www.w3.org/1999/xlink"
                        >
                            <mask id="mask0" mask-type="alpha">
                                <path d="M190.312 36.4879C206.582 62.1187 201.309 102.826 182.328 134.186C163.346 165.547 
                                130.807 187.559 100.226 186.353C69.6454 185.297 41.0228 161.023 21.7403 129.362C2.45775 
                                97.8511 -7.48481 59.1033 6.67581 34.5279C20.9871 10.1032 59.7028 -0.149132 97.9666 
                                0.00163737C136.23 0.303176 174.193 10.857 190.312 36.4879Z"/>
                            </mask>
                            <g mask="url(#mask0)">
                                <path d="M190.312 36.4879C206.582 62.1187 201.309 102.826 182.328 134.186C163.346 
                                165.547 130.807 187.559 100.226 186.353C69.6454 185.297 41.0228 161.023 21.7403 
                                129.362C2.45775 97.8511 -7.48481 59.1033 6.67581 34.5279C20.9871 10.1032 59.7028 
                                -0.149132 97.9666 0.00163737C136.23 0.303176 174.193 10.857 190.312 36.4879Z"/>
                                <image className="home__blob-img" x='12' y='18' 
                                xlinkHref={info.picture}
                                // href="/assets/img/perfil.png"
                                />
                            </g>
                        </svg>
                    </div>

                    <div className="home__data">
                        <h1 className="home__title"> {info.name} </h1>
                        <h3 className="home__subtitle">{info.profesion}</h3>
                        <p className="home__description"> {info.description} </p>
                        <p className="home__autor"> {info.autor} </p>
                        <Link
                            to={`contact`}
                            spy={true}
                            offset={0}
                            duration={500} 
                            className="button button--flex"
                        >
                              Contact Me  
                              <SendIcon className='button__icon' />
                            </Link>
                    </div>
                </div>

                <div className="home__scroll">
                    <Link
                        to={`about`}
                        spy={true}
                        offset={0}
                        duration={500} 
                        className="home__scroll-button button--flex"
                    >
                        <MouseIcon className='home__scroll-mouse'/>
                        <span className="home__scroll-name">
                            Scroll down 
                        </span>
                        <ArrowDownwardIcon className='home__scroll-arrow' />
                    </Link>
                </div>

            </div>
        </section>
    </>
  )
}
