import Image from 'next/image'
import {Swiper, SwiperSlide } from 'swiper/react';
import { Pagination, Navigation } from "swiper";

import {ArrowForwardOutlined} from '@mui/icons-material'



// Import Swiper styles
import "swiper/css";
import "swiper/css/bundle";
import "swiper/css/pagination";
import "swiper/css/navigation";

const URICloud = `https://res.cloudinary.com/dgekgbspr/image/upload/v1653599752/projects/`

const portfolio = [
    {
        // img: `assets/img/portfolio1.jpg`,
        img: `${URICloud}ihf9ajh28896crb3dysq.png`,
        name: `Online Store`,
        description: `Ecommerce - MongoDb - OAuth - Paypal - CRUD - middlewares`,
        link: `https://teslo-shop-ecomerce.herokuapp.com/`,
        time: `Jun 2022`,

    },
    {
        img: `${URICloud}hackernew_nz1abr.png`,
        // img: `assets/img/portfolio3.jpg`,
        name: `Challenge hacker new`,
        description: `Challenge, Api Consumption, local storage of favorites, paging`,
        link: `https://coruscating-sorbet-d38fea.netlify.app/`,
        time: `May 2022`,

    },
    {
        // img: `assets/img/portfolio2.jpg`,
        img: `${URICloud}openjira_t1juyo.png`,
        name: `Open Jira`,
        description: `Task App, CRUD, drag-and-drop functionality of the notes`,
        link: `https://open-jira-with-next-react-version-18.vercel.app/`,
        time: `Apr 2022`,

    },
   
    {
        // img: `assets/img/portfolio1.jpg`,
        img: `${URICloud}polemon_vs4kne.png`,
        name: `Pokemon App`,
        description: `pokemon API consumption, local storages, show favorites, developed in Nextjs`,
        link: `https://pokemon-app-nextjs-xi.vercel.app/`,
        time: `Apr 2022`,

    },
    {
        // img: `assets/img/portfolio1.jpg`,
        img: `${URICloud}funplay_srxwbh.png`,
        name: `FunPlay App`,
        description: `Software for children of the elementary basic section, which allows the reinforcement of basic operations through interactive games.`,
        link: `https://funplayv2.herokuapp.com/`,
        time: `Jan 2021`,

    },
]

export const PortfolioSection = () => {
  return (
    <>
         {/* <!--==================== PORTFOLIO ====================--> */}
         <section className="portfolio section" id="portfolio">
            <h2 className="section__title"> Portfolio </h2>
            <span className="section__subtitle"> Most recent work </span>

            <div
                className="portfolio__container container swiper-container"
            >
             <Swiper
                slidesPerView={1}
                spaceBetween={30}
                loop={true}
                pagination={{
                    clickable: true,
                }}
                navigation={true}
                modules={[Pagination, Navigation]}
                className="swiper-wrapper"
                >
                    {
                        portfolio.map( d => (
                            <SwiperSlide
                                key={d.name}
                            >
                            <div
                                className="portfolio__content grid swiper-slide"
                            >

                                    <img src={d.img} alt={d.name} className="portfolio__img"/>

                                    <div className="portfolio__data">
                                        <h3 className="portfolio__title"> {d.name} </h3>
                                        <p className="portfolio__description">
                                        {d.description}
                                        </p>
                                        <p className="portfolio__description">{d.time} </p>
                                        <a target={'_blank'} rel='noreferrer' href={d.link} className="button button--flex button--small portfolio__button">
                                            Demo
                                            <ArrowForwardOutlined className='button__icon' />

                                            {/* <i className="uil uil-arrow-right button__icon"></i> */}
                                        </a>
                                    </div>

                            </div>  
                            </SwiperSlide>
                        ))
                    }
                

       
            </Swiper>

               
            </div>

        </section>
    </>
  )
}
