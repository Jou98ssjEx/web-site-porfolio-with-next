import React, { useState } from 'react'
import ViewQuiltOutlinedIcon from '@mui/icons-material/ViewQuiltOutlined';

import ArrowForwardOutlinedIcon from '@mui/icons-material/ArrowForwardOutlined';

import CheckCircleOutlineOutlinedIcon from '@mui/icons-material/CheckCircleOutlineOutlined';
import CodeOutlinedIcon from '@mui/icons-material/CodeOutlined';
import BorderColorOutlinedIcon from '@mui/icons-material/BorderColorOutlined';

import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';

const services = [
    {
        icon: <ViewQuiltOutlinedIcon className='services__icon' />,
        name: `Web`,
        lastName: `Designer`,
        items: [
            {
                description: `Website layout`,
            },
            {
                description: `Interface design`,
            },
            {
                description: `Themes - fonts`,
            },
            {
                description: `Responsive design`,
            },
        ]
    },
    {
        icon: <CodeOutlinedIcon className='services__icon' />,
        name: `Frontend`,
        lastName: `Developer`,
        items: [
            {
                description: `I developer the interface`,
            },
            {
                description: `Consume API Rest`,
            },
            {
                description: `Handle Hooks in React`,
            },
            // {
            //     description: `I position your company brand`,
            // },
        ]
    },
    {
        icon: <BorderColorOutlinedIcon className='services__icon' />,
        name: `Backend`,
        lastName: `Developer`,
        items: [
            {
                description: `I developer endpoint`,
            },
            {
                description: `Handle Middlewares`,
            },
            {
                description: `json Web Token - Auth`,
            },
            // {
            //     description: `I position your company brand`,
            // },
        ]
    },
]

export const ServicesSection = () => {

    const [activeModal, setActiveModal] = useState(false);
    const [nameModal, setNameModal] = useState('');

    const handleModal = ( name: string ) => {
        if( nameModal !== name ){
            setNameModal(name);
        }
        setActiveModal(!activeModal);
    }

    
  return (
    <>
        {/* <!--==================== SERVICES ====================--> */}
        <section className="services section" id="services">
            <h2 className="section__title"> Services </h2>
            <span className="section__subtitle"> What i offer </span>

            <div className="services__container container grid">

                {
                    services.map( d =>(
                        <div className="services__content" key={d.name}>
                            <div className="">
                                {/* <ViewQuiltOutlinedIcon className='services__icon'/> */}
                                {d.icon}
                                <h3 className="services__tittle"> {d.name} <br/> {d.lastName} </h3>
                            </div>

                            <span 
                                className="button button--flex button--small button--link services__button"
                                onClick={() => handleModal(d.name)}
                            >
                                Viwe more 
                                <ArrowForwardOutlinedIcon className='button__icon'  />
                            </span>

                            <div 
                                className={(activeModal && d.name === nameModal) ? `services__modal active-modal`: `services__modal`}
                            >
                            {/* <div className="services__modal"> */}
                                <div className="services__modal-content">
                                    <h4 className="services__modal-title"> {d.name} <br/> {d.lastName} </h4>
                                    <CloseOutlinedIcon  
                                        className='services__modal-close'
                                        onClick={()=>handleModal(d.name)}
                                    />

                                    <ul className="services__modal-services grid">
                                        {
                                            d.items.map( (item, idx) => (
                                                <li className="services__modal-service" key={idx}>
                                                    <CheckCircleOutlineOutlinedIcon className='services__modal-icon' />
                                                    <p> {item.description} </p>
                                                </li>
                                            ))
                                        }
                                      
                                    </ul>

                                </div>
                            </div>

                        </div>
                    ))
                }

            </div>
        </section>
    </>
  )
}
