import React, { useState } from 'react'
import CodeOutlinedIcon from '@mui/icons-material/CodeOutlined';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import StorageOutlinedIcon from '@mui/icons-material/StorageOutlined';
import ViewQuiltOutlinedIcon from '@mui/icons-material/ViewQuiltOutlined';

const data = [
    {
        title: `Frontend Developer`,
        subTitle: ``,
        icon: <CodeOutlinedIcon className='skills__icon' />,
        items:[
            {
                skill: `JAVASCRIPT`,
                porcent:`90`,
            },
            {
                skill: `TYPESCRIPT`,
                porcent:`75`,
            },
            {
                skill: `REACT`,
                porcent:`80`,
            },
            {
                skill: `NEXTJS`,
                porcent:`70`,
            },
            
        ]
    },
    {
        title: `Backend Developer`,
        subTitle: ``,
        icon: <StorageOutlinedIcon className='skills__icon'/>,
        items:[
            {
                skill: `NodeJS`,
                porcent:`85`,
            },
            {
                skill: `MongoDb`,
                porcent:`75`,
            },
            {
                skill: `Firebase`,
                porcent:`65`,
            },
           
        ]
    },
    {
        title: `Designer Web`,
        subTitle: ``,
        icon: <ViewQuiltOutlinedIcon className='skills__icon'/>,
        items:[
            {
                skill: `HTML`,
                porcent:`90`,
            },
            {
                skill: `CSS`,
                porcent:`75`,
            },
            {
                skill: `Bootstrap`,
                porcent:`80`,
            },
            {
                skill: `Material UI`,
                porcent:`75`,
            },
        ]
    },
]

export const SkillsSection = () => {

    const [view, setView] = useState(true);
    const [nameSection, setNameSection] = useState('Frontend Developer');

    const handleView = ( title: string) => {
        
        if(nameSection !== title){
            setNameSection(title);
        }

       if(view && nameSection === title){
           setView(false);
           setNameSection('');
       }

       if (!view && nameSection !== title){
           setView(true);
           setNameSection(title);
       }

    }


  return (
    <>
         {/* <!--==================== SKILLS ====================--> */}
         <section className="skills section" id="skills">
            <h2 className="section__title">
                Skills
            </h2>
            <span className="section__subtitle">
                My technical level
            </span>

            <div className="skills__container container grid">
                {
                    data.map( d => (
                        <div
                            key={d.title}
                            
                        >
                              <div
                                key={d.title}
                                // className={( nameSection!==d.title) ? `skills__content skills__close `:`skills__content skills__open`}
                                className={ ( view && nameSection === d.title ) ? `skills__content skills__open` : `skills__close`}
                                // className={`skills__content skills__open`}
                              >
                                    <div className="skills__header">
                                        {d.icon}
                                        <div className="">
                                            <h1 className="skills__title">
                                                {d.title}
                                            </h1>
                                            <span className="skills__subtitle">
                                                {d.subTitle}
                                            </span>
                                        </div>

                                            <KeyboardArrowDownOutlinedIcon
                                                className='skills__arrow' onClick={()=>handleView(d.title)}
                                            />

                                    </div>
                                    {
                                        d.items.map( item =>(
                                            <div key={item.skill} className="skills__list grid">
                                                <div className="skills__data">
                                                    <div className="skills__titles">
                                                        <h3 className="skills__name"> {item.skill} </h3>
                                                        <span className="skills__number"> {`${item.porcent}%`} </span>
                                                    </div>
                                                    <div className="skills__bar"
                                                    >
                                                        <span 
                                                            className={`skills__percentage`}
                                                            style={{
                                                                width: `${item.porcent}%`
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        ))
                                    }
                              </div>
                        </div>
                    ))
                }
            </div>

        </section>
    </>
  )
}
