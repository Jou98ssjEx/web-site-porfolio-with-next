import React from 'react'
import { AboutSection, ContactSection, HomeSection, PortfolioSection, QuantificationSection, ServicesSection, SkillsSection } from './sections'
import { useEffect } from 'react';

export const PorfolioContent = () => {

 

  return (
    <>
    {/* <script src="/assets/js/swiper-bundle.min.js"></script> */}

        {/* <!--==================== HOME ====================--> */}
            <HomeSection />

        {/* <!--==================== ABOUT ====================--> */}
            <AboutSection />

        {/* <!--==================== SKILLS ====================--> */}
            <SkillsSection />

        {/* <!--==================== QUALIFICATION ====================--> */}
            <QuantificationSection />

        {/* <!--==================== SERVICES ====================--> */}
            <ServicesSection />

        {/* <!--==================== PORTFOLIO ====================--> */}
            <PortfolioSection />

        {/* <!--==================== PROJECT IN MIND ====================--> */}


        {/* <!--==================== TESTIMONIAL ====================--> */}

        {/* <!--==================== CONTACT ME ====================--> */}
        <ContactSection />

    </>
  )
}
