import React from 'react'

import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';

import {Link} from 'react-scroll'

const data = {
  name: `Joao`,
  profesion: `System Enginner`,
}

const socialNetwork = [
    {
        name: `facebook`,
        icon: <FacebookIcon />,
        link: `/`,
    },
    {
        name: `instagran`,
        icon: <InstagramIcon />,
        link: `/`,
    },
    {
        name: `twitter`,
        icon: <TwitterIcon />,
        link: `/`,
    },
]

const navLink = [
    {
        link: 'services',
        name: 'Services',
    },
    {
        link: 'portfolio',
        name: 'Portfolio',
    },
    {
        link: 'contact',
        name: 'Contactme',
    },
]

export const Footer = () => {
  return (
    <footer className="footer">
        <div className="footer__bg">
            <div className="footer__container container grid">
                <div className="">
                    <h1 className="footer__title"> {data.name} </h1>
                    <span className="footer__subtitle"> {data.profesion} </span>
                </div>

                <ul className="footer__links">
                    {
                        navLink.map(d => (
                            <li
                                key={d.name}
                            >
                                <Link
                                    // activeClass={``}
                                    to={d.link}
                                    spy={true}
                                    offset={-60}
                                    duration={500}
                                    className="footer__link"
                                >
                                    {d.name}
                                </Link>
                            </li>
                        ))
                    }
                  
                </ul>

                <div className="footer__socials">

                    {
                        socialNetwork.map( d => (
                            <a
                                key={d.name}
                                className={`footer__social`}
                                target='_blank'
                                rel=''
                            >
                                {d.icon}
                            </a>
                        ))
                    }
                   
                </div>
            </div>

            <p className="footer__copy">
                &#169; JouDev All right reserved.
                {/* &#169; Bendicode All right reserved. */}
            </p>
        </div>

    </footer>
  )
}
