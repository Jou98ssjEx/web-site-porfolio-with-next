import {
    HomeOutlined,
    PersonOutlineOutlined,
    PostAddOutlined,
    BusinessCenterOutlined,
    InsertPhotoOutlined ,
    SendOutlined,
    CloseOutlined,
    GridViewOutlined,
} from '@mui/icons-material'


import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import {Link} from 'react-scroll';

const navLink = [
    {
        link: 'home',
        name: 'Home',
        icon: <HomeOutlined className='nav__icon' />,
    },
    {
        link: 'about',
        name: 'About',
        icon: <PersonOutlineOutlined className='nav__icon' />,
    },
    {
        link: 'skills',
        name: 'Skills',
        icon: <PostAddOutlined className='nav__icon' />,
    },
    {
        link: 'services',
        name: 'Services',
        icon:  <BusinessCenterOutlined className='nav__icon' />,
    },
    {
        link: 'portfolio',
        name: 'Portfolio',
        icon: <InsertPhotoOutlined className='nav__icon' />,
    },
    {
        link: 'contact',
        name: 'Contactme',
        icon:  <SendOutlined className='nav__icon' />,
    },
]

const nameProject = 'Joao García'

export const Navbar = () => {

    const [visible, setVisible] = useState(false);

    const [theme, setTheme] = useState(true);

    const {asPath, push} = useRouter();

    const [linkStatus, setLinkStatus] = useState('');
    
    useEffect(() => {
        // console.log(asPath);
        setLinkStatus(asPath)
    }, [asPath])
    
    const navigateTo = ( url: string ) => {
        push(url);
    }


    const changeTheme = ( ) => {
        setTheme(!theme)

    }

    const handleChange = ( ) => {
        setVisible(!visible);

    }
        
  return (
    <nav className={`nav container`}>
        <a  className={`nav__logo`}> <b> {nameProject} </b> </a>

        <div 
            className={visible ? `nav__menu show-menu` : `nav__menu`} 
            id="nav-menu"
        >
            <ul className={`nav__list grid`}>
                {
                    navLink.map( data => (
                        <li
                            className={`nav__item`}
                            key={data.name}
                        >
                            <Link
                                activeClass={`nav__link active-link`}
                                to={data.link}
                                spy={true}
                                offset={-60}
                                duration={500}
                                className={`nav__link` }
                            >
                                {data.icon}
                                 {/* <PostAddOutlinedIcon className='nav__icon' /> */}
                                
                                {data.name}
                            </Link>
                           
                        </li>
                    ))
                }

             
            </ul>
            <CloseOutlined className='nav__close' id="nav-close" onClick={handleChange}/>

        </div>

            {/* <!-- Theme change button --> */}
        <div className="nav__btns">
            {/* {
                theme ? 
                <DarkModeIcon 
                    className='change-theme' 
                    id="theme-button" 
                    onClick={changeTheme}
                />
                :
                <LightModeOutlinedIcon 
                    className='change-theme' 
                    id="theme-button" 
                    onClick={changeTheme}
                />
            } */}

            <div className="nav__toggle" id="nav-toggle">
                <GridViewOutlined onClick={handleChange} />
            </div>
        </div>
    </nav>
  )
}
