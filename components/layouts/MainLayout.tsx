import Head from 'next/head'
import React, { FC, ReactNode } from 'react'
import { Footer, Navbar } from '../ui';

import ArrowUpwardOutlinedIcon from '@mui/icons-material/ArrowUpwardOutlined';

import Scroll , { Link, scroller } from 'react-scroll'
// import {use} from 'react-use';
import { useWindowScroll } from 'react-use'
import { useState, useEffect } from 'react';

interface Props {
    title: string;
    section   ?: string;
    description: string;
    children: ReactNode;
}

const l = 
          'about' ||
          'skills' ||
        //   'qualification' ||
          'services' ||
          'portfolio' ||
          'contact' 

/* A React component that is using the useEffect hook to set the positionY state to the y value of the
window scroll. */
export const MainLayout: FC<Props> = ({title, section, description, children}) => {

    const { y } = useWindowScroll();

    const [positionY, setPositionY] = useState(0);

    useEffect(() => {
      setPositionY(y)
    }, [y]);
    

  return (
    <>
        <Head>
            <title>
                { title }
            </title>

            <meta name="description" content={ description } />

            <meta name="og:title" content={ title }/>
            <meta name="og:description" content={ description }/>

            <meta name="viewport" content="width=device-width, initial-scale=1.0"/ >

        </Head>


            <header 
                className={ positionY >= 80 ?`header scroll-header`: `header`}
            >
                <Navbar />
            </header>

            <main className=''>
                { children }
            </main>

            <footer>
                <Footer />
            </footer>

            {/* <!--==================== SCROLL TOP ====================--> */}
            <Link 
                activeClass={`hidden-scroll`}
                to={ 'home' }
                spy={true}
                offset={-60}
                duration={500} 
                className="scrollup show-scroll" 
            >
                <ArrowUpwardOutlinedIcon className='scrollup__icon'/>
            </Link>
    </>
  )
}
